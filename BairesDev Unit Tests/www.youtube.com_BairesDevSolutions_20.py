import unittest
from selenium import webdriver

class UITestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome()

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def test_ui(self):
        self.driver.get('https://www.youtube.com/BairesDevSolutions%20')
        self.assertEqual(self.driver.title, 'BairesDev - Nearshore Software Development & Staff Augmentation')

if __name__ == '__main__':
    unittest.main()
        self.assertEqual(len(self.driver.find_elements(By.TAG_NAME, 'a')), 72)
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='https://www.youtube.com/': //a[@href='https://www.youtube.com/']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='https://accounts.google.com/ServiceLogin?service=youtube&uilel=3&passive=true&continue=https%3A%2F%2Fwww.youtube.com%2Fsignin%3Faction_handle_signin%3Dtrue%26app%3Ddesktop%26hl%3Den%26next%3Dhttps%253A%252F%252Fwww.youtube.com%252FBairesDevSolutions%252520&hl=en&ec=65620': //a[@href='https://accounts.google.com/ServiceLogin?service=youtube&uilel=3&passive=true&continue=https%3A%2F%2Fwww.youtube.com%2Fsignin%3Faction_handle_signin%3Dtrue%26app%3Ddesktop%26hl%3Den%26next%3Dhttps%253A%252F%252Fwww.youtube.com%252FBairesDevSolutions%252520&hl=en&ec=65620']
        # Selector for <a> with href='https://www.youtube.com/': //a[@href='https://www.youtube.com/']
        # Selector for <a> with href='https://www.youtube.com/': //a[@href='https://www.youtube.com/']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='https://www.youtube.com/feed/subscriptions': //a[@href='https://www.youtube.com/feed/subscriptions']
        # Selector for <a> with href='https://www.youtube.com/feed/you': //a[@href='https://www.youtube.com/feed/you']
        # Selector for <a> with href='https://www.youtube.com/feed/history': //a[@href='https://www.youtube.com/feed/history']
        # Selector for <a> with href='https://www.youtube.com/null': //a[@href='https://www.youtube.com/null']
        # Selector for <a> with href='https://www.youtube.com/BairesDevSolutions%20#': //a[@href='https://www.youtube.com/BairesDevSolutions%20#']
        # Selector for <a> with href='https://www.youtube.com/redirect?event=channel_header&redir_token=QUFFLUhqbGQzZ080MDJlSWI1Z2hEbTROOHNlTENQTWh2d3xBQ3Jtc0tuVGlvQVc5STFEckNlbHFOcHE1YTlsYlpnb0lWd0s1eTdvallMMjFmQUVwWU5OR1EyRE5RQlBfbnZBSXg3c1BRRjVRQkwtY1FWMnRzcmNMY1BiSmR5ZGtMUDk4TFFVdHM2bkpOVGlneXYwR0Q0SWx2Zw&q=https%3A%2F%2Fbaires.dev%2FYoutube': //a[@href='https://www.youtube.com/redirect?event=channel_header&redir_token=QUFFLUhqbGQzZ080MDJlSWI1Z2hEbTROOHNlTENQTWh2d3xBQ3Jtc0tuVGlvQVc5STFEckNlbHFOcHE1YTlsYlpnb0lWd0s1eTdvallMMjFmQUVwWU5OR1EyRE5RQlBfbnZBSXg3c1BRRjVRQkwtY1FWMnRzcmNMY1BiSmR5ZGtMUDk4TFFVdHM2bkpOVGlneXYwR0Q0SWx2Zw&q=https%3A%2F%2Fbaires.dev%2FYoutube']
        # Selector for <a> with href='javascript:void(0);': //a[@href='javascript:void(0);']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='https://www.youtube.com/watch?v=xpj7ZupdgEI': //a[@href='https://www.youtube.com/watch?v=xpj7ZupdgEI']
        # Selector for <a> with href='': //a[@href='']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='https://www.youtube.com/watch?v=xpj7ZupdgEI': //a[@href='https://www.youtube.com/watch?v=xpj7ZupdgEI']
        # Selector for <a> with href='https://www.youtube.com/hashtag/bairesdev': //a[@href='https://www.youtube.com/hashtag/bairesdev']
        # Selector for <a> with href='https://www.youtube.com/hashtag/wearehere': //a[@href='https://www.youtube.com/hashtag/wearehere']
        # Selector for <a> with href='https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbjl1VklpRHBVT05kUlRlTnFhR0J1SXM2WkxyQXxBQ3Jtc0tsUnJjTlFacFBiLXFpcGkwR2s4d2tQRmxRTS1pb1hON2RyZ1FYZlJ4a2xRdDhjVGNUX1M5VXZjVURXNXBoTFVKb1R1VGtpLXRveFRuQVpDb3VPd0syV1dLTS1DaHJXVVRXZGZRNTFyaGdYQmJQUXNzSQ&q=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2Fbairesdev': //a[@href='https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbjl1VklpRHBVT05kUlRlTnFhR0J1SXM2WkxyQXxBQ3Jtc0tsUnJjTlFacFBiLXFpcGkwR2s4d2tQRmxRTS1pb1hON2RyZ1FYZlJ4a2xRdDhjVGNUX1M5VXZjVURXNXBoTFVKb1R1VGtpLXRveFRuQVpDb3VPd0syV1dLTS1DaHJXVVRXZGZRNTFyaGdYQmJQUXNzSQ&q=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2Fbairesdev']
        # Selector for <a> with href='https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbmZqVk9lbWpiOFhQTlgtRWMxRGxLRjdEWlpzd3xBQ3Jtc0tteFRoYzBZUVNaYmFYRVFKQko0YUZFb0hWQlFZZ3VXcHVib1hRNVAydW5IcTItNVJlaGxNcHJVakh2NkQxMGxUX1RqamNvYUROSkdGRmNSQThHOFBIRFVJREY1a3FVNV9OQi1iUUFiNmtUeXlFcUQ1dw&q=https%3A%2F%2Ftwitter.com%2Fbairesdev': //a[@href='https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbmZqVk9lbWpiOFhQTlgtRWMxRGxLRjdEWlpzd3xBQ3Jtc0tteFRoYzBZUVNaYmFYRVFKQko0YUZFb0hWQlFZZ3VXcHVib1hRNVAydW5IcTItNVJlaGxNcHJVakh2NkQxMGxUX1RqamNvYUROSkdGRmNSQThHOFBIRFVJREY1a3FVNV9OQi1iUUFiNmtUeXlFcUQ1dw&q=https%3A%2F%2Ftwitter.com%2Fbairesdev']
        # Selector for <a> with href='https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbmhhWjMyWGhESi1PakpsSloxYzNRNHI4SFJtd3xBQ3Jtc0ttVjVSbHR1MzcwZTlXZmZOQkhmODV5SVJEYmg0WG10c19vUjBGUWNuUFdVRng5VS03MEhCQkI3blpXYlRPXzZTeE5DcVJiZDlXaGVIcm9iQnZMeUtWNTl1bEZVRFAyTHdOWXBDZFNnalZsNTBYQUlZdw&q=https%3A%2F%2Fwww.instagram.com%2Fbairesdev': //a[@href='https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbmhhWjMyWGhESi1PakpsSloxYzNRNHI4SFJtd3xBQ3Jtc0ttVjVSbHR1MzcwZTlXZmZOQkhmODV5SVJEYmg0WG10c19vUjBGUWNuUFdVRng5VS03MEhCQkI3blpXYlRPXzZTeE5DcVJiZDlXaGVIcm9iQnZMeUtWNTl1bEZVRFAyTHdOWXBDZFNnalZsNTBYQUlZdw&q=https%3A%2F%2Fwww.instagram.com%2Fbairesdev']
        # Selector for <a> with href='https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqa3pPdzNnOF9sT1ZMVFJpNEZnSW9tdmI4TFZkUXxBQ3Jtc0trekpYdWc1eWV2aU9fSFJfOVZUY1Y3RExfR2plaGJhamJ5ckh5SG9zWGt1N1J2amw1b2RXMjk1TzhCX3NZWEdHemxQWFNDUE81ZTVpbWhXYkstSEdaeVVRemV6alNmdXltQjc5ZDk5T2lGSXZqYUhLWQ&q=https%3A%2F%2Fwww.facebook.com%2Fbairesdev': //a[@href='https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqa3pPdzNnOF9sT1ZMVFJpNEZnSW9tdmI4TFZkUXxBQ3Jtc0trekpYdWc1eWV2aU9fSFJfOVZUY1Y3RExfR2plaGJhamJ5ckh5SG9zWGt1N1J2amw1b2RXMjk1TzhCX3NZWEdHemxQWFNDUE81ZTVpbWhXYkstSEdaeVVRemV6alNmdXltQjc5ZDk5T2lGSXZqYUhLWQ&q=https%3A%2F%2Fwww.facebook.com%2Fbairesdev']
        # Selector for <a> with href='https://www.youtube.com/watch?v=xpj7ZupdgEI': //a[@href='https://www.youtube.com/watch?v=xpj7ZupdgEI']
        # Selector for <a> with href='https://www.youtube.com/channel/UCOS6u5b1gazP6BdwVTmOAhA/videos?view=0&sort=dd&shelf_id=0': //a[@href='https://www.youtube.com/channel/UCOS6u5b1gazP6BdwVTmOAhA/videos?view=0&sort=dd&shelf_id=0']
        # Selector for <a> with href='https://www.youtube.com/channel/UCOS6u5b1gazP6BdwVTmOAhA/videos?view=0&sort=dd&shelf_id=0': //a[@href='https://www.youtube.com/channel/UCOS6u5b1gazP6BdwVTmOAhA/videos?view=0&sort=dd&shelf_id=0']
        # Selector for <a> with href='https://www.youtube.com/watch?v=wPhKWGuCjdQ&list=UULFOS6u5b1gazP6BdwVTmOAhA': //a[@href='https://www.youtube.com/watch?v=wPhKWGuCjdQ&list=UULFOS6u5b1gazP6BdwVTmOAhA']
        # Selector for <a> with href='https://www.youtube.com/watch?v=wPhKWGuCjdQ': //a[@href='https://www.youtube.com/watch?v=wPhKWGuCjdQ']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='https://www.youtube.com/watch?v=wPhKWGuCjdQ': //a[@href='https://www.youtube.com/watch?v=wPhKWGuCjdQ']
        # Selector for <a> with href='https://www.youtube.com/watch?v=xpj7ZupdgEI': //a[@href='https://www.youtube.com/watch?v=xpj7ZupdgEI']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='https://www.youtube.com/watch?v=xpj7ZupdgEI': //a[@href='https://www.youtube.com/watch?v=xpj7ZupdgEI']
        # Selector for <a> with href='https://www.youtube.com/watch?v=1owJVwm0INg': //a[@href='https://www.youtube.com/watch?v=1owJVwm0INg']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='https://www.youtube.com/watch?v=1owJVwm0INg': //a[@href='https://www.youtube.com/watch?v=1owJVwm0INg']
        # Selector for <a> with href='https://www.youtube.com/watch?v=TQZ5Spki6QQ': //a[@href='https://www.youtube.com/watch?v=TQZ5Spki6QQ']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='https://www.youtube.com/watch?v=TQZ5Spki6QQ': //a[@href='https://www.youtube.com/watch?v=TQZ5Spki6QQ']
        # Selector for <a> with href='https://www.youtube.com/watch?v=s8QJvdlksQ4': //a[@href='https://www.youtube.com/watch?v=s8QJvdlksQ4']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='https://www.youtube.com/watch?v=s8QJvdlksQ4': //a[@href='https://www.youtube.com/watch?v=s8QJvdlksQ4']
        # Selector for <a> with href='https://www.youtube.com/watch?v=Zqoa0pMtrl4': //a[@href='https://www.youtube.com/watch?v=Zqoa0pMtrl4']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='https://www.youtube.com/watch?v=Zqoa0pMtrl4': //a[@href='https://www.youtube.com/watch?v=Zqoa0pMtrl4']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='': //a[@href='']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        # Selector for <a> with href='None': //a[@href='None']
        self.assertEqual(len(self.driver.find_elements(By.CLASS_NAME, 'some-class')), 0)
