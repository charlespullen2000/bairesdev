import unittest
from selenium import webdriver

class UITestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome()

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def test_ui(self):
        self.driver.get('https://jobs.bairesdev.com/')
        self.assertEqual(self.driver.title, 'BairesDev - Nearshore Software Development & Staff Augmentation')

if __name__ == '__main__':
    unittest.main()
        self.assertEqual(len(self.driver.find_elements(By.TAG_NAME, 'a')), 119)
        # Selector for <a> with href='https://www.bairesdev.com/': //a[@href='https://www.bairesdev.com/']
        # Selector for <a> with href='https://www.bairesdev.com/software-development-services/': //a[@href='https://www.bairesdev.com/software-development-services/']
        # Selector for <a> with href='https://www.bairesdev.com/software-development-services/': //a[@href='https://www.bairesdev.com/software-development-services/']
        # Selector for <a> with href='https://www.bairesdev.com/staff-augmentation/': //a[@href='https://www.bairesdev.com/staff-augmentation/']
        # Selector for <a> with href='https://www.bairesdev.com/software-development-services/software-dedicated-team/': //a[@href='https://www.bairesdev.com/software-development-services/software-dedicated-team/']
        # Selector for <a> with href='https://www.bairesdev.com/software-development-services/software-outsourcing/': //a[@href='https://www.bairesdev.com/software-development-services/software-outsourcing/']
        # Selector for <a> with href='https://www.bairesdev.com/react/': //a[@href='https://www.bairesdev.com/react/']
        # Selector for <a> with href='https://www.bairesdev.com/nodejs/': //a[@href='https://www.bairesdev.com/nodejs/']
        # Selector for <a> with href='https://www.bairesdev.com/technologies/python-software-development/': //a[@href='https://www.bairesdev.com/technologies/python-software-development/']
        # Selector for <a> with href='https://www.bairesdev.com/net/': //a[@href='https://www.bairesdev.com/net/']
        # Selector for <a> with href='https://www.bairesdev.com/java/': //a[@href='https://www.bairesdev.com/java/']
        # Selector for <a> with href='https://www.bairesdev.com/ruby/': //a[@href='https://www.bairesdev.com/ruby/']
        # Selector for <a> with href='https://www.bairesdev.com/technologies/outsource-php-development/': //a[@href='https://www.bairesdev.com/technologies/outsource-php-development/']
        # Selector for <a> with href='https://www.bairesdev.com/technologies/go/': //a[@href='https://www.bairesdev.com/technologies/go/']
        # Selector for <a> with href='https://www.bairesdev.com/angular/': //a[@href='https://www.bairesdev.com/angular/']
        # Selector for <a> with href='https://www.bairesdev.com/technologies/': //a[@href='https://www.bairesdev.com/technologies/']
        # Selector for <a> with href='https://www.bairesdev.com/software-development-services/expertise/qa-services-software-testing/': //a[@href='https://www.bairesdev.com/software-development-services/expertise/qa-services-software-testing/']
        # Selector for <a> with href='https://www.bairesdev.com/bi/': //a[@href='https://www.bairesdev.com/bi/']
        # Selector for <a> with href='https://www.bairesdev.com/software-development-services/expertise/big-data-analytics/': //a[@href='https://www.bairesdev.com/software-development-services/expertise/big-data-analytics/']
        # Selector for <a> with href='https://www.bairesdev.com/technologies/ios-development/': //a[@href='https://www.bairesdev.com/technologies/ios-development/']
        # Selector for <a> with href='https://www.bairesdev.com/software-development-services/expertise/ui-ux-design/': //a[@href='https://www.bairesdev.com/software-development-services/expertise/ui-ux-design/']
        # Selector for <a> with href='https://www.bairesdev.com/android/': //a[@href='https://www.bairesdev.com/android/']
        # Selector for <a> with href='https://www.bairesdev.com/machine-learning/': //a[@href='https://www.bairesdev.com/machine-learning/']
        # Selector for <a> with href='https://www.bairesdev.com/technologies/database-development-services/': //a[@href='https://www.bairesdev.com/technologies/database-development-services/']
        # Selector for <a> with href='https://www.bairesdev.com/software-development-services/front-end/': //a[@href='https://www.bairesdev.com/software-development-services/front-end/']
        # Selector for <a> with href='https://www.bairesdev.com/software-development-services/expertise/': //a[@href='https://www.bairesdev.com/software-development-services/expertise/']
        # Selector for <a> with href='https://www.bairesdev.com/clients/': //a[@href='https://www.bairesdev.com/clients/']
        # Selector for <a> with href='https://www.bairesdev.com/about/': //a[@href='https://www.bairesdev.com/about/']
        # Selector for <a> with href='https://www.bairesdev.com/about/': //a[@href='https://www.bairesdev.com/about/']
        # Selector for <a> with href='https://www.bairesdev.com/about/leadership-team/': //a[@href='https://www.bairesdev.com/about/leadership-team/']
        # Selector for <a> with href='https://www.bairesdev.com/about/our-development-teams/': //a[@href='https://www.bairesdev.com/about/our-development-teams/']
        # Selector for <a> with href='https://www.bairesdev.com/hire-software-developers/': //a[@href='https://www.bairesdev.com/hire-software-developers/']
        # Selector for <a> with href='https://www.bairesdev.com/awards-recognitions/': //a[@href='https://www.bairesdev.com/awards-recognitions/']
        # Selector for <a> with href='https://www.bairesdev.com/diversity-inclusion/': //a[@href='https://www.bairesdev.com/diversity-inclusion/']
        # Selector for <a> with href='https://www.bairesdev.com/software-development-services/expertise/': //a[@href='https://www.bairesdev.com/software-development-services/expertise/']
        # Selector for <a> with href='https://www.bairesdev.com/blog/': //a[@href='https://www.bairesdev.com/blog/']
        # Selector for <a> with href='https://www.bairesdev.com/join-us/': //a[@href='https://www.bairesdev.com/join-us/']
        # Selector for <a> with href='https://www.bairesdev.com/join-us/': //a[@href='https://www.bairesdev.com/join-us/']
        # Selector for <a> with href='https://jobs.bairesdev.com/': //a[@href='https://jobs.bairesdev.com/']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/': //a[@href='https://www.bairesdev.com/referrals-program/']
        # Selector for <a> with href='https://www.bairesdev.com/start/basic-details/': //a[@href='https://www.bairesdev.com/start/basic-details/']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/?job_position=Junior%20QA%20Analyst%20/%20R%20+%20D%20-%20Remote%20Work': //a[@href='https://www.bairesdev.com/referrals-program/?job_position=Junior%20QA%20Analyst%20/%20R%20+%20D%20-%20Remote%20Work']
        # Selector for <a> with href='https://applicants.bairesdev.com/job/27/175563/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite': //a[@href='https://applicants.bairesdev.com/job/27/175563/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/?job_position=QA%20Analyst%20Senior%20/%20Research%20+%20Development%20-%20Remote%20work': //a[@href='https://www.bairesdev.com/referrals-program/?job_position=QA%20Analyst%20Senior%20/%20Research%20+%20Development%20-%20Remote%20work']
        # Selector for <a> with href='https://applicants.bairesdev.com/job/27/177816/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite': //a[@href='https://applicants.bairesdev.com/job/27/177816/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/?job_position=Big%20Data%20Architect%20-%20Remote%20Work': //a[@href='https://www.bairesdev.com/referrals-program/?job_position=Big%20Data%20Architect%20-%20Remote%20Work']
        # Selector for <a> with href='https://applicants.bairesdev.com/job/99/261079/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite': //a[@href='https://applicants.bairesdev.com/job/99/261079/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/?job_position=.NET%20+%20Angular%20Engineer%20/%20Research%20+%20Development%20-%20Remote%20Work': //a[@href='https://www.bairesdev.com/referrals-program/?job_position=.NET%20+%20Angular%20Engineer%20/%20Research%20+%20Development%20-%20Remote%20Work']
        # Selector for <a> with href='https://applicants.bairesdev.com/job/1/246755/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite': //a[@href='https://applicants.bairesdev.com/job/1/246755/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/?job_position=.Net%20+%20Angular%20Fullstack%20%20/%20Research%20+%20Development%20-REMOTE%20WORK': //a[@href='https://www.bairesdev.com/referrals-program/?job_position=.Net%20+%20Angular%20Fullstack%20%20/%20Research%20+%20Development%20-REMOTE%20WORK']
        # Selector for <a> with href='https://applicants.bairesdev.com/job/1/182702/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite': //a[@href='https://applicants.bairesdev.com/job/1/182702/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/?job_position=Scala%20Architect%20-%20Remote%20Work': //a[@href='https://www.bairesdev.com/referrals-program/?job_position=Scala%20Architect%20-%20Remote%20Work']
        # Selector for <a> with href='https://applicants.bairesdev.com/job/100/261102/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite': //a[@href='https://applicants.bairesdev.com/job/100/261102/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/?job_position=.NET%20Engineer%20/Research%20+%20Development%20-%20Remote%20Work': //a[@href='https://www.bairesdev.com/referrals-program/?job_position=.NET%20Engineer%20/Research%20+%20Development%20-%20Remote%20Work']
        # Selector for <a> with href='https://applicants.bairesdev.com/job/1/182940/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite': //a[@href='https://applicants.bairesdev.com/job/1/182940/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/?job_position=Senior%20Sharepoint%20Developer%20|LATAM|%20-%20Remote%20Work': //a[@href='https://www.bairesdev.com/referrals-program/?job_position=Senior%20Sharepoint%20Developer%20|LATAM|%20-%20Remote%20Work']
        # Selector for <a> with href='https://applicants.bairesdev.com/job/85/261125/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite': //a[@href='https://applicants.bairesdev.com/job/85/261125/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/?job_position=React%20Native%20Developer%20-%20Remote%20Work': //a[@href='https://www.bairesdev.com/referrals-program/?job_position=React%20Native%20Developer%20-%20Remote%20Work']
        # Selector for <a> with href='https://applicants.bairesdev.com/job/198/178008/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite': //a[@href='https://applicants.bairesdev.com/job/198/178008/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/?job_position=Software%20Engineering%20Manager%20-%20Remote%20Work': //a[@href='https://www.bairesdev.com/referrals-program/?job_position=Software%20Engineering%20Manager%20-%20Remote%20Work']
        # Selector for <a> with href='https://applicants.bairesdev.com/job/49/255838/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite': //a[@href='https://applicants.bairesdev.com/job/49/255838/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/?job_position=Angular%20Engineer%20-%20Remote%20Work': //a[@href='https://www.bairesdev.com/referrals-program/?job_position=Angular%20Engineer%20-%20Remote%20Work']
        # Selector for <a> with href='https://applicants.bairesdev.com/job/615/261248/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite': //a[@href='https://applicants.bairesdev.com/job/615/261248/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/?job_position=React%20Engineer%20-%20Remote%20Work': //a[@href='https://www.bairesdev.com/referrals-program/?job_position=React%20Engineer%20-%20Remote%20Work']
        # Selector for <a> with href='https://applicants.bairesdev.com/job/169/86990/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite': //a[@href='https://applicants.bairesdev.com/job/169/86990/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/?job_position=Junior/MidLevel%20Java%20Developer%20-%20Remote%20Work': //a[@href='https://www.bairesdev.com/referrals-program/?job_position=Junior/MidLevel%20Java%20Developer%20-%20Remote%20Work']
        # Selector for <a> with href='https://applicants.bairesdev.com/job/3/176651/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite': //a[@href='https://applicants.bairesdev.com/job/3/176651/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/?job_position=SemiSenior%20Data%20Analytics%20/%20R+D%20-%20Remote%20Work': //a[@href='https://www.bairesdev.com/referrals-program/?job_position=SemiSenior%20Data%20Analytics%20/%20R+D%20-%20Remote%20Work']
        # Selector for <a> with href='https://applicants.bairesdev.com/job/1028/261219/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite': //a[@href='https://applicants.bairesdev.com/job/1028/261219/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/?job_position=UX%20Content%20Designer%20/%20R+D%20-%20Remote%20Work': //a[@href='https://www.bairesdev.com/referrals-program/?job_position=UX%20Content%20Designer%20/%20R+D%20-%20Remote%20Work']
        # Selector for <a> with href='https://applicants.bairesdev.com/job/66/261196/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite': //a[@href='https://applicants.bairesdev.com/job/66/261196/apply?utm_source=websitebairesdev&utm_medium=erp&utm_campaign=jobssite']
        # Selector for <a> with href='https://jobs.bairesdev.com/#list': //a[@href='https://jobs.bairesdev.com/#list']
        # Selector for <a> with href='https://jobs.bairesdev.com/#list': //a[@href='https://jobs.bairesdev.com/#list']
        # Selector for <a> with href='https://jobs.bairesdev.com/#list': //a[@href='https://jobs.bairesdev.com/#list']
        # Selector for <a> with href='https://jobs.bairesdev.com/#list': //a[@href='https://jobs.bairesdev.com/#list']
        # Selector for <a> with href='https://jobs.bairesdev.com/#list': //a[@href='https://jobs.bairesdev.com/#list']
        # Selector for <a> with href='https://jobs.bairesdev.com/#list': //a[@href='https://jobs.bairesdev.com/#list']
        # Selector for <a> with href='https://jobs.bairesdev.com/#list': //a[@href='https://jobs.bairesdev.com/#list']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/': //a[@href='https://www.bairesdev.com/referrals-program/']
        # Selector for <a> with href='https://www.bairesdev.com/join-us/': //a[@href='https://www.bairesdev.com/join-us/']
        # Selector for <a> with href='https://www.bairesdev.com/about/': //a[@href='https://www.bairesdev.com/about/']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/': //a[@href='https://www.bairesdev.com/referrals-program/']
        # Selector for <a> with href='https://www.bairesdev.com/about/': //a[@href='https://www.bairesdev.com/about/']
        # Selector for <a> with href='https://www.bairesdev.com/about/methodology/': //a[@href='https://www.bairesdev.com/about/methodology/']
        # Selector for <a> with href='https://www.bairesdev.com/technologies/': //a[@href='https://www.bairesdev.com/technologies/']
        # Selector for <a> with href='https://www.bairesdev.com/about/certifications-and-partnerships/': //a[@href='https://www.bairesdev.com/about/certifications-and-partnerships/']
        # Selector for <a> with href='https://www.bairesdev.com/software-development-services/': //a[@href='https://www.bairesdev.com/software-development-services/']
        # Selector for <a> with href='https://www.bairesdev.com/software-development-services/software-dedicated-team/': //a[@href='https://www.bairesdev.com/software-development-services/software-dedicated-team/']
        # Selector for <a> with href='https://www.bairesdev.com/staff-augmentation/': //a[@href='https://www.bairesdev.com/staff-augmentation/']
        # Selector for <a> with href='https://www.bairesdev.com/software-development-services/expertise/': //a[@href='https://www.bairesdev.com/software-development-services/expertise/']
        # Selector for <a> with href='https://www.bairesdev.com/software-development-services/software-outsourcing/': //a[@href='https://www.bairesdev.com/software-development-services/software-outsourcing/']
        # Selector for <a> with href='https://www.bairesdev.com/diversity-inclusion/': //a[@href='https://www.bairesdev.com/diversity-inclusion/']
        # Selector for <a> with href='https://www.bairesdev.com/csr/': //a[@href='https://www.bairesdev.com/csr/']
        # Selector for <a> with href='https://www.bairesdev.com/continuity/': //a[@href='https://www.bairesdev.com/continuity/']
        # Selector for <a> with href='https://www.bairesdev.com/success-stories/': //a[@href='https://www.bairesdev.com/success-stories/']
        # Selector for <a> with href='https://www.bairesdev.com/blog/': //a[@href='https://www.bairesdev.com/blog/']
        # Selector for <a> with href='https://www.bairesdev.com/press-media/': //a[@href='https://www.bairesdev.com/press-media/']
        # Selector for <a> with href='https://www.bairesdev.com/software-development-insights/': //a[@href='https://www.bairesdev.com/software-development-insights/']
        # Selector for <a> with href='https://www.bairesdev.com/technology-insights/': //a[@href='https://www.bairesdev.com/technology-insights/']
        # Selector for <a> with href='https://www.bairesdev.com/industry-insights/': //a[@href='https://www.bairesdev.com/industry-insights/']
        # Selector for <a> with href='https://www.bairesdev.com/tech-resource-center/': //a[@href='https://www.bairesdev.com/tech-resource-center/']
        # Selector for <a> with href='https://www.bairesdev.com/referral-partners': //a[@href='https://www.bairesdev.com/referral-partners']
        # Selector for <a> with href='https://www.bairesdev.com/join-us/': //a[@href='https://www.bairesdev.com/join-us/']
        # Selector for <a> with href='https://jobs.bairesdev.com/': //a[@href='https://jobs.bairesdev.com/']
        # Selector for <a> with href='https://www.bairesdev.com/referrals-program/': //a[@href='https://www.bairesdev.com/referrals-program/']
        # Selector for <a> with href='https://www.bairesdev.com/landing/go/basic-details': //a[@href='https://www.bairesdev.com/landing/go/basic-details']
        # Selector for <a> with href='https://www.bairesdev.com/contact-us/': //a[@href='https://www.bairesdev.com/contact-us/']
        # Selector for <a> with href='tel:+14084782739': //a[@href='tel:+14084782739']
        # Selector for <a> with href='https://www.bairesdev.com/privacy-policy/': //a[@href='https://www.bairesdev.com/privacy-policy/']
        # Selector for <a> with href='https://www.linkedin.com/company/bairesdev': //a[@href='https://www.linkedin.com/company/bairesdev']
        # Selector for <a> with href='https://www.facebook.com/bairesdev/': //a[@href='https://www.facebook.com/bairesdev/']
        # Selector for <a> with href='https://twitter.com/bairesdev': //a[@href='https://twitter.com/bairesdev']
        # Selector for <a> with href='https://www.instagram.com/bairesdev/': //a[@href='https://www.instagram.com/bairesdev/']
        # Selector for <a> with href='https://www.youtube.com/BairesDevSolutions%20': //a[@href='https://www.youtube.com/BairesDevSolutions%20']
        # Selector for <a> with href='https://www.bairesdev.com/privacy-policy/': //a[@href='https://www.bairesdev.com/privacy-policy/']
        # Selector for <a> with href='https://www.bairesdev.com/terms-conditions/': //a[@href='https://www.bairesdev.com/terms-conditions/']
        # Selector for <a> with href='https://www.privacyshield.gov/participant?id=a2zt0000000GwXYAA0&status=Active': //a[@href='https://www.privacyshield.gov/participant?id=a2zt0000000GwXYAA0&status=Active']
        # Selector for <a> with href='https://www.bairesdev.com/privacy-policy': //a[@href='https://www.bairesdev.com/privacy-policy']
        # Selector for <a> with href='https://www.bairesdev.com/privacy-policy': //a[@href='https://www.bairesdev.com/privacy-policy']
        self.assertEqual(len(self.driver.find_elements(By.CLASS_NAME, 'some-class')), 0)
