import unittest
from selenium import webdriver

class UITestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome()

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def test_ui(self):
        self.driver.get('https://bairesdev.mo.cloudinary.net/coresite/NextRoll_BairesDev_landscape_version.mp4')
        self.assertEqual(self.driver.title, 'BairesDev - Nearshore Software Development & Staff Augmentation')

if __name__ == '__main__':
    unittest.main()
        self.assertEqual(len(self.driver.find_elements(By.TAG_NAME, 'a')), 0)
        self.assertEqual(len(self.driver.find_elements(By.CLASS_NAME, 'some-class')), 0)
